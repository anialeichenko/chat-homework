import db from 'diskdb';

db.connect(`./data`, [
    "users",
    "sessions",
    "messages",
]);

export default db;