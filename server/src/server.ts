import express from 'express';
import cors from 'cors';
import http from 'http';
import bodyParser from 'body-parser';
import { Server } from 'socket.io';
import socketHandler from './socket';
import routes from './routes';
import { PORT } from './config';

const app = express();
const httpServer = new http.Server(app);
const socketIo = new Server(httpServer, {
    cors: {
        origin: "*",
        methods: ["GET", "POST"]
    }
});

const corsOptions = {
    origin:'*', 
    credentials: true,          
    optionSuccessStatus:200,
 };
 app.use(cors(corsOptions));

 app.use(bodyParser.json());

routes(app);

app.get('users/edit', (req, res) => res.send('222'))

socketHandler(socketIo);

httpServer.listen(PORT, () => console.log(`Listen server on port ${PORT}`));

export default { app, httpServer };
