import { Server, Socket } from 'socket.io';
import userModel from '../models/userModel';
import messageModel from '../models/messageModel';

export default (io: Server) => {
	io.on('connection', socket => {
		const sessionId = socket.handshake.query.sessionId;
		const user = userModel.getBySessionId(sessionId);
		console.log(user);
		socket.emit('MESSAGES', messageModel.getList());
		socket.on('NEW_MESSAGE', (message) => {
			messageModel.add(message);
			io.emit('MESSAGES', messageModel.getList());
		})

		socket.on('REMOVE_MESSAGE', (id) => {
			const message = messageModel.get(id);
			if (message?.userId === user._id) {
				messageModel.remove(id);
				io.emit('MESSAGES', messageModel.getList());
			}
		})

		socket.on('UPDATE_MESSAGE', ({ id, text }) => {
			const message = messageModel.get(id);
			if (message.userId === user._id) {
				messageModel.updateText(id, text);
				io.emit('MESSAGES', messageModel.getList());
			}
		})

		socket.on('LIKE_MESSAGE', (id) => {
			const message = messageModel.get(id);
			if (message?.userId !== user._id) {
				messageModel.like(id, user);
				io.emit('MESSAGES', messageModel.getList());
			}
		})
	});
	
};
