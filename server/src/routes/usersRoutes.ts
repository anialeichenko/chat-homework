import { Router } from 'express';
import db from '../db';
import userModel from '../models/userModel';


const router = Router();

router.get('/', (req, res) => {
	if (userModel.checkIsAdminBySessionId(req.get('sessionId')) === false) {
		res.status(403).json({ error: 'Access Deny' });
		return;
	}
	const result = db.users.find()
	res.status(200).json({ data: result });
});

router.get('/:id', (req, res) => {
	if (userModel.checkIsAdminBySessionId(req.get('sessionId')) === false) {
		res.status(403).json({ error: 'Access Deny' });
		return;
	}
	const result = db.users.findOne({_id: req.params.id})
	res.status(200).json({ data: result });
});

router.post('/edit', (req, res) => {
	if (userModel.checkIsAdminBySessionId(req.get('sessionId')) === false) {
		res.status(403).json({ error: 'Access Deny' });
		return;
	}
    if(req.body.id) {
		db.users.update({_id: req.body.id}, {
			login: req.body.userName,
			password: req.body.userPassword,
		})
	} else {
		db.users.save({
			login: req.body.userName,
			password: req.body.userPassword,
		});
    }
	res.json({});
});

router.post('/delete', (req, res) => {
	if (userModel.checkIsAdminBySessionId(req.get('sessionId')) === false) {
		res.status(403).json({ error: 'Access Deny' });
		return;
	}
	db.users.remove({_id: req.body.id}, false);
	res.status(200).json({});
});

export default router;