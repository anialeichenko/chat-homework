import loginRoutes from './loginRoutes';
import usersRoutes from './usersRoutes';
import { Express } from 'express';

export default (app: Express) => {
	app.use('/login', loginRoutes);
	app.use('/users', usersRoutes);
};
