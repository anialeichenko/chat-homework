import { Router } from 'express';
import db from '../db';
import userModel from '../models/userModel';

const router = Router();

router.post('/', (req, res) => {
	const user = db.users.findOne({login: req.body.userName, password: req.body.userPassword});
	if (user) {
		const session = db.sessions.save({
			userId: user._id,
		});
		res.json({
			sessionId: session._id,
			isAdmin: userModel.checkIsAdmin(user),
		});
	} else {
		res.status(401).json({
			error: 'Not Found',
		});
	}
});

router.get('/session', (req, res) => {
   const user = userModel.getBySessionId(req.get('sessionId'));
   if (user) {
	   user.isAdmin = userModel.checkIsAdmin(user);
   }
   res.json({ user });
});

export default router;
