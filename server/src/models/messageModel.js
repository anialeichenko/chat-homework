import db from '../db';

const add = (message) => {
    return db.messages.save(message);
}

const get = (id) => {
    return db.messages.findOne({_id: id})
}

const getList = () => {
    return db.messages.find();
}
const remove = (id) => {
    db.messages.remove({_id: id});
}

const updateText = (id, text) => {
    db.messages.update({_id: id}, {
        text,
    })
}

const like = (id, user) => {
    const message = get(id);
    let likes = message.likes || [];

    if (!likes.includes(user._id)) {
        likes.push(user._id);
    } else {
        likes = likes.filter(userId => userId !== user._id);
    }

    db.messages.update({_id: id}, {
        likes,
    })
}

export default {
    get,
    add,
    remove,
    getList,
    updateText,
    like,
}