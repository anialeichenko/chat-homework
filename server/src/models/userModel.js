import db from '../db';
import { admins } from '../config';

const getBySessionId = (sessionId) => {
    const session = db.sessions.findOne({_id: sessionId });
    if (!session) {
        return;
    }
    const user = db.users.findOne({_id: session.userId});
    return user;
}

const checkIsAdmin = (user) => {
    return admins.includes(user.login);
}

const checkIsAdminBySessionId = (sessionId) => {
    const user = getBySessionId(sessionId);
    if (!user) {
        return false;
    }
    return admins.includes(user.login);
}

export default {
    getBySessionId,
    checkIsAdmin,
    checkIsAdminBySessionId,
}