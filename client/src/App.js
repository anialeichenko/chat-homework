import Chat from "./components/Chat/Chat";
import Login from "./components/Login/Login";
import Users from "./components/Users/Users";
import { Routes, Route } from "react-router-dom";
import UsersEdit from "./components/UsersEdit/UsersEdit";
import Init from "./components/Init";


function App() {
  return (
    <div className="App">
       <Init />
       <Routes>
        <Route path="/" element={<Chat url='https://edikdolynskyi.github.io/react_sources/messages.json' />} />
        <Route path="login" element={<Login />} />
        <Route path="users" element={<Users />} />
        <Route path="users/edit/:id" element={<UsersEdit />} />
        <Route path="users/edit" element={<UsersEdit />} />
      </Routes>
    </div>
  );
}

export default App;
