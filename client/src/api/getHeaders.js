export default function getHeaders(headers = {}) {
    return {
       sessionId: window.sessionStorage.getItem('sessionId'),
       'Content-Type': 'application/json',
       ...headers,
    }
}