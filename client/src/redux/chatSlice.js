import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  messages: [],
}

export const chatSlice = createSlice({
  name: 'chat',
  initialState,
  reducers: {
    setMessages: (state, action) => {
      state.messages = action.payload
    },
  },
})

// Action creators are generated for each case reducer function
export const { setMessages } = chatSlice.actions

export default chatSlice.reducer