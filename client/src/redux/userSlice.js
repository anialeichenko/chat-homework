import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  user: null,
  isLoaded: false,
}

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setUser: (state, action) => {
      state.user = action.payload
    },
    userLoaded: (state) => {
      state.isLoaded = true;
    },
  },
})

// Action creators are generated for each case reducer function
export const { setUser, userLoaded } = userSlice.actions

export default userSlice.reducer