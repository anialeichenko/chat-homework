import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles( (theme) => ({
  hr: {
    textAlign: "center",
    position: "absolute",
    width: "100%",
    left: 0,
    top: "-10px",
  },  
  line: {
    borderTop: "2px solid blue",
    position: "relative",
    margin: "24px 0",
  },
  messageDate: {
    backgroundColor: "white",
    display: "inline-block",
  },
}));

