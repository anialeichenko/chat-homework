import  Message  from "../Message/Message";
import OwnMessage from "../OwnMessage/OwnMessage";
import { useStyles } from './styles';

function formatDate(date) {
  const dateStr = new Intl.DateTimeFormat('en-US', { 
    weekday: 'long',
    day: '2-digit',
    month: 'long',
  }).format(date);

  const today = new Date();
  const todayStr = new Intl.DateTimeFormat('en-US', { 
    weekday: 'long',
    day: '2-digit',
    month: 'long',
  }).format(today);

  if (todayStr === dateStr) {
    return 'Today';
  }

  const yesterday = new Date(today)
  yesterday.setDate(yesterday.getDate() - 1);
  const yesterdayStr = new Intl.DateTimeFormat('en-US', { 
    weekday: 'long',
    day: '2-digit',
    month: 'long',
  }).format(yesterday);

  if (yesterdayStr === dateStr) {
    return 'Yesterday';
  }
  const [weekDay, month, day ] = dateStr.split(' ');
  return `${weekDay} ${day} ${month}`;
}

function MessageList({ messages, clickLikeHandler, myUserId, deleteHandler, updateValueOwnMessage }) {
  const classes = useStyles();

  return (
    <div className="message-list">
      {messages.map((message, index) => {      
        const messageComponent = (() => {
          if (message.userId === myUserId) {
            return (
              <OwnMessage message={message} deleteHandler={deleteHandler} updateValueOwnMessage={updateValueOwnMessage}></OwnMessage>
            );
          }
          return (
            <Message message={message} clickLikeHandler={clickLikeHandler} myUserId={myUserId} />
          );
        })();

        const hrComponent = (() => {
          const messageDate = new Date(message.createdAt);
          const messageDateStr = formatDate(messageDate);

          const prevMessage = messages[index - 1];
          if (!prevMessage) {
            return (
              <div className={classes.line}>
                <div className={classes.hr}>
                  <div className={classes.messageDate}>
                    {messageDateStr}
                  </div>
                </div> 
              </div>
            )
          }
          const prevMessageDate = new Date(prevMessage.createdAt);
          const prevMessageDateStr = formatDate(prevMessageDate);

          if (prevMessageDateStr === messageDateStr) {
            return null;
          }
          return (
            <div className={classes.line}>
              <div className={classes.hr}>
                <div className={classes.messageDate}>
                  {messageDateStr}
                </div>
              </div> 
            </div>
          )
        })();
        
        return (
          <div key={message._id}>
            <div className="messages-divider">
              {hrComponent}
            </div>
            {messageComponent}
          </div>
        );
      })}
    </div>
  );
}

export default MessageList;