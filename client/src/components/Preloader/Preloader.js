import * as React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import { useStyles } from './styles';

function Preloader() {
  const classes = useStyles();
 
  return (
    <div className={`${classes["preloader"]} preloader`}>
      <CircularProgress disableShrink />
    </div>
  );
}

export default Preloader;
