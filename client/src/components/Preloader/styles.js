import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(() => ({
    "preloader": {
      position: "fixed",
      top: 0,
      left: 0,
      width: "100%",
      height: "100%",
      display: "flex",
      alignItems: "center",
      justifyContent: "center", 
    },
  }));