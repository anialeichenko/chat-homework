import React from 'react';
import { useState, useEffect } from "react";
import { useStyles } from "./styles";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { useSelector } from 'react-redux';

export default function ColorTextFields() {
  const classes = useStyles();
  const [ userName, setUserName ] = useState("");
  const [ userPassword, setUserPassword ] = useState("");
  const user = useSelector((state) => state.user.user);
  const userLoaded = useSelector((state) => state.user.isLoaded);

  useEffect(() => {
    if (userLoaded && user) {
      window.location.href = '/';
    }
  }, [userLoaded, user]);

  function onChangeLogin (event) {
    setUserName(event.target.value)
  }
  
  function onChangePassword (event) {
    setUserPassword(event.target.value)
  }

  function clickSignIn () {
      fetch('http://localhost:3002/login', {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            userName,
            userPassword,
        }),
      })
      .then(res => res.json())
      .then(({ sessionId, isAdmin }) => {
        window.sessionStorage.setItem('sessionId', sessionId);
        if (isAdmin) {
          window.location.href = '/users';
        } else {
          window.location.href = '/';
        }
      });
  }


    return (
      <div className={classes.root}>
        <div className={classes.margin}>
          <form noValidate autoComplete="off">
            <TextField id="login" label="Login" color="primary" onChange={onChangeLogin} />
          </form>
        </div>
        <div className={classes.margin}>
          <form noValidate autoComplete="off">
            <TextField id="password" label="Password" type="password" color="primary" onChange={onChangePassword} />
          </form>
        </div>
        <div className={classes.margin}>
          <Button variant="contained" color="primary" onClick={clickSignIn} >
            Sign in
          </Button>
        </div>
      </div>
    );
}