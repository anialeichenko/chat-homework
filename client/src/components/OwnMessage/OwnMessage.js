import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import {BsFillPencilFill, BsFillTrashFill } from "react-icons/bs";
import { useState, useEffect } from 'react';
import  { dateFormat } from "../dateFormat";
import Button from '@material-ui/core/Button';
import { useStyles } from './styles';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

function OwnMessage({ 
  message, 
  deleteHandler = () => {},
  updateValueOwnMessage = () => {},
}) {
  const classes = useStyles();
  const [messageDate, setMessageDate] = useState('');
  const [messageText, setMessageText] = useState(message.text);
  const [isEditing, setIsEditing] = useState(false);

  useEffect(() => {
    const date = new Date(message.createdAt); 
    setMessageDate(dateFormat(date));
  },[message.createdAt])
  
  const handleClickOpen = () => {
    setIsEditing(true);
  }

  const handleClose = () => {
    setIsEditing(false);
  };
  
  const onChangeHandler = (event) => {
    setMessageText(event.target.value);
  };
  

  const onClickSaveHandler = () => {
    updateValueOwnMessage(message._id, messageText);
    setIsEditing(false);
  }

  return (
      <Card className={classes.root}> 
          <CardContent className={`${classes["own-message"]} own-message`}>
            <img alt={message.user} src={message.avatar} className={classes.img}></img>
            <div>
              <h3>
                {message.user}
              </h3>
            
              <p className={`${classes["message-text"]} message-text`}>
                {message.text}
              </p>
          
              <div className={`${classes["message-time"]} message-time`}>
                {messageDate}
              </div>
              <div className={classes.buttons}>
                {!isEditing && (
                  <div>
                  <button className={`${classes.button} message-edit`} onClick={handleClickOpen}>
                    <BsFillPencilFill></BsFillPencilFill>
                  </button>
                  </div>
                )}
                <button className={`${classes.button} message-delete`} onClick={() => deleteHandler(message._id)}>
                  <BsFillTrashFill></BsFillTrashFill>
                </button>
              </div>
            </div>

            <Dialog open={isEditing} onClose={handleClose} aria-labelledby="form-dialog-title">
              <DialogTitle id="form-dialog-title">Edit Message</DialogTitle>
              <DialogContent>
                <TextField
                  autoFocus
                  margin="dense"
                  id="name"
                  label="your text"
                  type="text"
                  value={messageText}
                  onChange={onChangeHandler}
                />
              </DialogContent>
              <DialogActions>
                <Button onClick={handleClose} color="primary">
                  Cancel
                </Button>
                <Button onClick={onClickSaveHandler} color="primary">
                  Save
                </Button>
              </DialogActions>
            </Dialog>
          </CardContent>
      </Card>
  );
}

export default OwnMessage;