import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles( (theme) => ({
    root: {
      margin: theme.spacing(1),
      background: 'lightgray',
      width: "500px",
      marginLeft: "400px",
    },
    "own-message": {
      display: 'flex',
    },
    img: {
      width: '100px',
      height: '100px',
      margin: theme.spacing(1),
    },
    buttons: {
      display: "flex",
      marginTop: "10px",
    },
    button: {
      marginLeft: "5px",  
      "&:hover, &:focus": {
        color: "blue",
        curson: "pointer",
      }
    },
  }));