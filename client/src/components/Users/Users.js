import React from 'react';
import { useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import { Link } from "react-router-dom";
import Card from '@material-ui/core/Card';
import { useNavigate } from "react-router-dom";
import getHeaders from "../../api/getHeaders";
import { useStyles } from "./styles";

export default function ContainedButtons() {
  const classes = useStyles();
  const [ users, setUsers ] = useState([]);
  let navigate = useNavigate();

  function clickDelete (id) {
    fetch('http://localhost:3002/users/delete', {
      method: "POST",
      headers: getHeaders(),
      body: JSON.stringify({
        id,
      }),
    })
    .then(() => setUsers(users.filter(user => user._id !== id)));
  } 

  function clickEdit (id) {
    navigate(`/users/edit/${id}`)
  } 
  
  useEffect(() => {
    fetch('http://localhost:3002/users', {
      headers: getHeaders(),
    })
    .then(res => res.json())
    .then(res => {
      if(res.error) throw Error(res.error);
      setUsers(res.data);
    });
  }, []);

  return (
    <div className={classes.root}>
      <Link to="/users/edit">
          <Button variant="contained" color="primary">
              Create user
          </Button>
      </Link>
      {users.map((user) => (
        <div key={user._id}>
          <Card className={classes.card}> 
            <h3 className={classes.margin}>
              {user.login}
            </h3>
            <div>
            <Button variant="contained" color="primary" onClick={() => clickEdit(user._id)} className={classes.margin}>
              Edit
            </Button>
            <Button variant="contained" color="primary" onClick={() => clickDelete(user._id)} className={classes.margin}>
              Delete
            </Button>
            </div>
          </Card>
        </div>
      ))}
    </div>
  );
}