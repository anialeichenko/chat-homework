import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
    card: {
      width: "500px",
      display: "flex",
      alignItems: "center",
      justifyContent: "space-between",
      backgroundColor: "lightgrey",
      marginTop: "20px",
    },
    margin: {
      margin: "10px",
    },
  }))