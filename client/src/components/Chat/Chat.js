import { useEffect } from "react";
import Header from "../Header/Header";
import  MessageInput  from "../MessageInput/MessageInput";
import MessageList from "../MessageList/MessageList";
import { useStyles } from './styles';
import Preloader from "../Preloader/Preloader";
import { useSelector } from 'react-redux';

function Chat() {
  const classes = useStyles();
  const messages = useSelector((state) => state.chat.messages);
  const user = useSelector((state) => state.user.user);
  const userLoaded = useSelector((state) => state.user.isLoaded);
  
  useEffect(() => {
    if (userLoaded && !user) {
      window.location.href = '/login';
    }
  }, [userLoaded, user]);

  const clickLikeHandler = (id) => {
     window.socket?.emit('LIKE_MESSAGE', id);
  }

  const deleteHandler = (id) => {
    window.socket?.emit('REMOVE_MESSAGE', id);
  }

  const onMessage = (message) => {
    window.socket?.emit("NEW_MESSAGE", message)
  }

  const updateValueOwnMessage = (id , messageText) => {
    window.socket?.emit('UPDATE_MESSAGE', { id, text: messageText });
  }

  if(!userLoaded) {
    return <Preloader></Preloader>
  }

  return (
    <div className="chat">
      <Header messages={messages} user={user}></Header>
      <main className={classes.main}>
        <MessageList 
          messages={messages} 
          clickLikeHandler={clickLikeHandler} 
          deleteHandler={deleteHandler} 
          myUserId={user?._id} 
          updateValueOwnMessage={updateValueOwnMessage}
        >
        </MessageList>
      </main>
      <footer>
        <MessageInput onMessage={onMessage} user={user} />
      </footer>
    </div>
  );
}

export default Chat;