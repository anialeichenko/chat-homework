import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(() => ({
    main: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        marginTop: "40px",
    },
}));