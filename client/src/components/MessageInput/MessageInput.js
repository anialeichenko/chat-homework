import React from 'react';
import { useState } from 'react';
import Button from '@material-ui/core/Button';
import { useStyles } from './styles';

export default function MessageInput({ onMessage, user }) {
  const classes = useStyles();
  const [ownText, setOwnText] = useState("");

  const onSubmitSend = (event) => {
    event.preventDefault(); 
    onMessage({
      userId: user._id,
      user: user.login,
      avatar: "https://avatarko.ru/img/avatar/5/chelovechek_4158.jpg",
      text: ownText,
      createdAt: String(new Date()),
    });
    setOwnText("");
  }

  const onChangeHandler = (event) => {
    setOwnText(event.target.value);
  }
  
  return (
    <div>
      <form className={`${classes["message-input"]} message-input`} noValidate autoComplete="off" onSubmit={onSubmitSend}>
        <input type="text" placeholder="Type message here..." className={`${classes["message-input-text"]} message-input-text`} onChange={onChangeHandler}value={ownText} />
        <Button variant="contained" color="primary" type="submit" className="message-input-button">
          Send
        </Button>
      </form>
    </div>
  );
}
