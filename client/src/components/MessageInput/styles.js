import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
    "message-input": {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        marginBottom: "16px",
    },
    "message-input-text": {
      height: '16px',
      width: '800px',
      marginRight: '8px',
      padding: '8px',
    },
}));
