import React from 'react';
import { useState, useEffect } from "react";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { useNavigate } from "react-router-dom";
import { useParams } from "react-router-dom";
import getHeaders from "../../api/getHeaders";
import { useStyles } from "./styles";

export default function UsersEdit() {
  const classes = useStyles();
  const [ userName, setUserName ] = useState("");
  const [ userPassword, setUserPassword ] = useState("");
  let navigate = useNavigate();
  let params = useParams();

  useEffect(() => {
    fetch(`http://localhost:3002/users/${params.id}`, {
      headers: getHeaders(),
    })
    .then(res => res.json())
    .then(res => {
      if(res.error) throw Error(res.error);
      setUserName(res.data.login);
      setUserPassword(res.data.password);
    });
  }, [params])

  function onChangeLogin (event) {
    setUserName(event.target.value)
  }
  
  function onChangePassword (event) {
    setUserPassword(event.target.value)
  }
  
  function clickSave () {
    if(userName !== "" && userPassword !== "") {
      fetch('http://localhost:3002/users/edit', {
          method: "POST",
          headers: getHeaders(),
          body: JSON.stringify({
              userName,
              userPassword,
              id: params.id,
          }),
      })
      .then(navigate("/users"))
    } else { 
      return console.error("Enter value");
    }
  }

  return (
    <form className={classes.root} noValidate autoComplete="off">
      <TextField id="standard-basic" label="Login" onChange={onChangeLogin} value={userName} />
      <TextField id="standard-basic" label="Password" onChange={onChangePassword} type="password" value={userPassword} />
      <Button variant="contained" color="primary" onClick={clickSave} >
        Save
      </Button>
    </form>
  );
}