import React from 'react';
import { useState, useEffect } from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { BsFillHandThumbsUpFill } from "react-icons/bs";
import  { dateFormat } from "../dateFormat";
import { useStyles } from './styles';

export default function Message({ 
  message, 
  clickLikeHandler = () => {},
  myUserId,
}) {
  const classes = useStyles();
  const [messageDate, setMessageDate] = useState('');

  useEffect(() => {
    const date = new Date(message.createdAt); 
    setMessageDate(dateFormat(date));
  },[message.createdAt])
  

  return (
      <Card className={classes.root}> 
          <CardContent className={`${classes["message"]} message`}>
            <img alt={message.user} src={message.avatar} className={`${classes["message-user-avatar"]} message-user-avatar`}></img>
            <div>
              <h3 className={`${classes["message-user-name"]} message-user-name`}>
                {message.user}
              </h3>
              <p className={`${classes["message-text"]} message-text`}>
                {message.text}
              </p>
              <div className={`${classes["message-time"]} message-time`}>
                {messageDate}
              </div>
                {message.likes ? message.likes.length : 0}
                <BsFillHandThumbsUpFill 
                  style={{
                    color: message?.likes?.includes(myUserId) ? 'blue' : 'black',
                  }} 
                  className={`${classes["message-like"]} ${message.liked ? 'message-liked' : 'message-like'}`} 
                  onClick={() => clickLikeHandler(message._id)}
                >
                </BsFillHandThumbsUpFill>
            </div>
          </CardContent>
      </Card>
  );
}
