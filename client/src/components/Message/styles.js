import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles( (theme) => ({
    root: {
      margin: theme.spacing(1),
      background: 'lightgray',
      width: "500px",
      marginRight: "400px",
    },
    "message": {
      display: 'flex',
    },
    "message-user-avatar": {
      width: '100px',
      height: '100px',
      margin: theme.spacing(1),
    },
    "message-like": {
      "&:hover, &:focus": {
      cursor: 'pointer',
      color: 'blue',
      }
    },
  }));