import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
    "header": {
      flexGrow: 1,
    },
    "header-title": {
      flexGrow: 1,
    },
    headerInfo: {
      margin: theme.spacing(1),
    },
  }));