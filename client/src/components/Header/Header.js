import React from 'react';
import { useState, useEffect } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { dateFormat } from '../dateFormat';
import { useStyles } from './styles';

export default function Header({messages, user}) {
  const classes = useStyles();
  const [usersCount, setUsersCount] = useState(0);
  const lastElement = messages.length - 1;
  let lastMessage;
  
  
  if(messages.length) {
    lastMessage = dateFormat(new Date(messages[lastElement].createdAt));
  }

  useEffect(() => {
    const usersSet = new Set();
    messages.map((item) => (
      usersSet.add(item.userId)
    ));
    setUsersCount(usersSet.size);
  },[messages])


  return (
    <div className={`${classes["header"]} header`}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" className={`${classes["header-title"]} header-title`}>
            My Chat ({user?.login})
          </Typography>
          <div className={`${classes.headerInfo} header-users-count`}>
              {usersCount + " users"}
          </div>
          <div className={`${classes.headerInfo} header-messages-count`}>
              {messages.length + " messages"}
          </div>
          <div className={`${classes.headerInfo} header-last-message-date`}>
            {"last message at " + lastMessage}
          </div>
        </Toolbar>
      </AppBar>
    </div>
  );
}
