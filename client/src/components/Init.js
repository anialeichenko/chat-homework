import { useEffect } from "react";
import { useDispatch } from 'react-redux';
import openSocket from 'socket.io-client';

import { setUser, userLoaded } from '../redux/userSlice';
import { setMessages } from '../redux/chatSlice';
import getHeaders from "../api/getHeaders";


export default function Init () {
    const dispatch = useDispatch();

    useEffect(() => {
        const sessionId = window.sessionStorage.getItem('sessionId');
        fetch('http://localhost:3002/login/session', {
            headers: getHeaders(),
        })
        .then(res => res.json())
        .then(res => { 
            dispatch(setUser(res.user));
            window.socket = openSocket('http://localhost:3002', { query: { sessionId } });
            window.socket.on("MESSAGES", (messages) => {
                dispatch(setMessages(messages));
            }) 
        }).finally(() => {
            dispatch(userLoaded());
        });
    }, [ dispatch ]);
    return null;
}